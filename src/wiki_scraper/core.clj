(ns wiki-scraper.core
  (:gen-class :main true)
  (:use [clojure.tools.cli :only (cli)])
  (:require [wiki-scraper.scraper :as scraper]))

(defn run
  [opts args]
  (scraper/plot (get opts :month) (get opts :year)))

(defn -main [& args]
  (let [[opts args banner]
        (cli args
             ["-h" "--help" "Show help" :flag true :default false]
             ["-x" "--month" "Set the month" :default false]
             ["-y" "--year" "Set the year" :default false])]
    (when (:help opts)
      (println banner)
      (System/exit 0))
    (if
        (and
         (:month opts)
         (:year opts))
      (do
        (println "")
        (run opts args))
      (println banner))))