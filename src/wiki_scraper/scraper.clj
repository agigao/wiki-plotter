(ns wiki-scraper.scraper
  (:require [clojure.data.json :as json]
            [clj-time.coerce :as convert])
  (:use [incanter core io datasets stats charts]))

(defn fetch-data
  [date]
  (try
    (->> (str "http://stats.grok.se/json/en/" date "/web_scraping")
         (slurp)
         (json/read-json)
         (:daily_views))
       (catch Exception e (println "Wrong Date/URL:" (.getMessage e))
                               (System/exit 0))))

(defn plot
  [month year]
  (let [date (if (> (read-string month) 9)
               (str year month)
               (str year "0" month))
        data     (fetch-data date)
        dates    (->> (keys data)
                      (map name)
                      (map convert/to-long))
        dataset  (->> (to-dataset dates)
                      (conj-cols (vals data))
                      (rename-cols {:col-0 :Date
                                    :col-1 :View}))]
    (view (time-series-plot :View :Date
                           :x-label "Date"
                           :y-label "View"
                           :title "Web_Scraping Views @ Wikipedia"
                           :data dataset))))
