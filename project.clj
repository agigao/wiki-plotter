(defproject wiki-scraper "0.1.0-SNAPSHOT"
  :description "Fetch and Graph stats for Wikipedia pages "
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/tools.cli "0.3.1"]
                 [incanter/incanter "1.5.7"]
                 [clj-time "0.12.2"]]
  :main ^:skip-aot wiki-scraper.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
