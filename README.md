# Wiki-Plotter

Nothing special. just a trivial homework assignment, code reads JSON from remote url(wiki page stats) and plots a time-series graph. 

## Installation

Download and install [https://leiningen.org](https://leiningen.org)

macOS shortcut for installation:
`brew install leiningen`

## Jar

Cooking distributable standalon jar is as simple as that
 
 `lein uberjar`


## Options

cd into project directory and
`lein run`
displays different options to provide

 ```bash
  Switches               Default  Desc          
  --------               -------  ----          
  -h, --no-help, --help  false    Show help     
  -x, --month            false    Set the month 
  -y, --year             false    Set the year  
 ```

## Examples

```lein run -x 3 -y 2006```

```java -jar wiki-scraper-0.1.0-SNAPSHOT-standalone.jar -x 5 -y 1994```

## License

Copyright © 2016 Giga Chokheli